# Vagrant : MongoDB

Création et configuration d'une VM Linux Centos 7.1 avec MongoDB 3.2 à l'aide de Ansible

## Accès

* Port 27017 : MongoDB

## commandes

    # Démarrage VM
    $> vagrant up
    # Arrêt VM
    $> vagrant halt
    # Connexion à la VM
    $> vagrant ssh
    # Reconfiguration
    $> vagrant reload --provision
    # Suppression
    $> vagrant destroy -f

## Remarques ##

* Vagrant 1.8.1 : bug dans la gestion de `ansible_local` sous Windows (#6757)
    - Corrigé dans la v1.8.2
    - Contournement détaillé dans l'ano sur Github (patch script Ruby Vagrant)
